import axios from 'axios'

const getData = async function (city) {
  try {
    const { data } = await axios.get(
      `http://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${process.env.VUE_APP_ID}&units=metric`
    )
    return {
      id: new Date().getUTCMilliseconds(),
      city: city,
      country: data.sys.country,
      weather: data.weather[0].main,
      temperature: data.main.temp,
      humidity: data.main.humidity,
      date: new Date()
    }
  } catch (e) {
    console.log(e)
  }
}

const saveData = (data) => {
  localStorage.setItem('weathes', JSON.stringify(data))
}

export { getData, saveData }
